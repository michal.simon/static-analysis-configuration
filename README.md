# Static Analysis Configuration

[![Codeac.io](https://static.codeac.io/badges/3-21003301.svg "Codeac.io")](https://app.codeac.io/gitlab/michal.simon/static-analysis-configuration)

This is a sample repository for testing static analysis configuration for React Frontend written in TypeScript.

## Linting

```bash
npm run lint
```
