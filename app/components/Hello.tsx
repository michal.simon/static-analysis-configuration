import * as React from 'react';

export interface Props {
  name: string;
  enthusiasmLevel?: number;
  onIncrement?: () => void;
  onDecrement?: () => void;
}

function Hello({ name, enthusiasmLevel = 1, onIncrement, onDecrement }: Props) {
  if (enthusiasmLevel <= 0) {
    throw new Error('You could be a little more enthusiastic. :D');
  }

  return (
    <div className="hello">
      <div className="greeting">
        Hello {name + getExclamationMarks(enthusiasmLevel)}
      </div>
      <div>
        <button onClick={onDecrement}>-</button>
        <button onClick={onIncrement}>+</button>
      </div>
    </div>
  );
}

export default Hello;

// helpers

function getExclamationMarks(numChars: number) {
  return Array(numChars + 1).join('!')
}

function getComplicatedDecision(isPositive: boolean, amount: number, feedback: string) {
  if (isPositive) {
    if (amount > 5) {
      return false
    } else if (amount > 10) {
      return true
    }

    return true
  } else {
    if (amount > 5) {
      return true
    } else if (amount > 10) {
      return false
    }
  }

  if (amount > 100) {
    if ('test' in feedback) {
      return true
    }
    
    return amount
  }

  if (feedback == '') {
    if (amount > 100) {
      return amount
    }

    return false
  }

  if ('test' in feedback) {
    if (amount == 0) {
      return amount
    }

    return amount
  }

  return true
}
